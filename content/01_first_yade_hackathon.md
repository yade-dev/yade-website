+++
chapter = true
menuPre = "<b>X. </b>"
title = "1st Yade hackathon"
weight = 5
+++

### News

# 1st Yade hackathon

On 23. and 24 of June 2022 in Freiberg, Germany the first Yade Hackathon took place!

8 developers were on site and 1 joined online during the presentation sessions.
Developers could finally meet offline after the two pandemic years, exchange thoughts,
discuss issues in the software, prognose the future of the project, fix bugs, work
on the code and have a good time together.

We were glad that the former but one of the core developer Vaclav could join us to have
very pleasent technical and social communication.

Main topics which were discussed:

- Install documentation update. [MR](https://gitlab.com/yade-dev/trunk/-/merge_requests/870)
- Dropping of the wiki page and moving the valuable information to the documentation or on the website. [Issue](https://gitlab.com/yade-dev/trunk/-/issues/283)
- Check the last publications, where the Yade was sited and put the links into the documentation. [MR](https://gitlab.com/yade-dev/trunk/-/merge_requests/875) 
- Split installation dependencies, depending on the required features.[MR](https://gitlab.com/yade-dev/trunk/-/merge_requests/870)
- ADd support of the Qt6, which is already available in Debian repositories [Issue](https://gitlab.com/yade-dev/trunk/-/issues/279). FollowUp actions are identified
- Distribution of the yade-dem.org domain permissions to increase the bus factor for the project.
- Fixing the newly added gitlab runner nova1. [MR](https://gitlab.com/yade-dev/trunk/-/issues/266)
- Presenting the last works, based on the Yade and newer features added in the source code recently.
- Drop google-analytics code from the website. [Issue](https://gitlab.com/yade-dev/trunk/-/issues/280), [MR](https://gitlab.com/yade-dev/trunk/-/merge_requests/873).
- A lot of technical and non-technical discussions.

The positive side of offline meetings is the opportunity to discuss and make decisions very quickly.
Discussing the problems per email, issue tracker sometimes takes days, weeks, month.... Having the
 discussion parner just right here accelerates a the way!

We want to thank everybody who made this event possible:
- [TU Bergakademie Freiberg](https://tu-freiberg.de/) for a general support.
- [Institute for Informatics](https://tu-freiberg.de/fakult1/inf) of the TU Bergakademie Freiberg, and personally [Christian Schubert](https://tu-freiberg.de/user/983), [Birgit Steffen](https://tu-freiberg.de/user/319) and [Sebastian Zug](https://tu-freiberg.de/fakult1/inf/professuren/softwaretechnologie-und-robotik).
- [Institute for the processing machines and recycling system technick](https://tu-freiberg.de/fakult4/iart), TU Bergakademie Freiberg, and personally Dr.-Ing. Prof. [Holger Lieberwirth](https://www.linkedin.com/in/lieberwirth)
- [Institute of dynamics and flow mechanics](https://tu-freiberg.de/fakult4/imfd), TU Bergakademie Freiberg, and personally Dr.-Ing. Prof. [Rüdiger Schwarze](https://www.linkedin.com/in/rüdiger-schwarze-9b197764)
- [Haver Engineering GmbH](https://www.haverengineering.de), and personally [Jan Lampke](https://www.linkedin.com/in/jan-lampke-b92881118) and [Hagen Müller](https://www.linkedin.com/in/dr-hagen-m%C3%BCller-19ba0053/).

Staff which need to be fixed for the next time to make Hackathon more effective:
- Plan much more time for the hacking. Ideally - much more days and till the night
- Find better place to be reached by plane, making an event more attractive for wider circle of participants (Frankfurt-Am-Main)
